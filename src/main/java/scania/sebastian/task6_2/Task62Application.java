package scania.sebastian.task6_2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Task62Application {

    public static void main(String[] args) {
        SpringApplication.run(Task62Application.class, args);
    }

}
