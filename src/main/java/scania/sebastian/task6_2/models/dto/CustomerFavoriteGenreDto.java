package scania.sebastian.task6_2.models.dto;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@EqualsAndHashCode
public class CustomerFavoriteGenreDto {
    private int customerId;
    private String firstName;
    private String lastName;
    private String genre;
    private int genreCount;
}
