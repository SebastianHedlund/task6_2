package scania.sebastian.task6_2.models.dto;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@EqualsAndHashCode
public class CustomerShortDto {
    private int customerId;
    private String firstName;
    private String lastName;
    private String Country;
    private String Postal_code;
    private String phone;
    private String email;

    public CustomerShortDto(String firstName, String lastName, String country, String postal_code, String phone, String email) {
        this.firstName = firstName;
        this.lastName = lastName;
        Country = country;
        Postal_code = postal_code;
        this.phone = phone;
        this.email = email;
    }

    public CustomerShortDto() {
    }
}
