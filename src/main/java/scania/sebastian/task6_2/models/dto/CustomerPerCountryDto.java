package scania.sebastian.task6_2.models.dto;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@EqualsAndHashCode
public class CustomerPerCountryDto {
    private String country;
    private int totalCustomers;
}
