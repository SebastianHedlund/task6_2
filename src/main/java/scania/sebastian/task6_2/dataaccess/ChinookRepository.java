package scania.sebastian.task6_2.dataaccess;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import scania.sebastian.task6_2.models.dto.CustomerShortDto;
import scania.sebastian.task6_2.models.dto.CustomerCountryDto;
import scania.sebastian.task6_2.models.dto.CustomerFavoriteGenreDto;
import scania.sebastian.task6_2.models.dto.CustomerSpendingsDto;
import scania.sebastian.task6_2.models.dto.CustomerPerCountryDto;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Service
public class ChinookRepository {
    @Value("${spring.datasource.url}")
    private String url;

    public List<CustomerShortDto> getCustomers(){
        String sql = "SELECT c.customer_id, c.first_name, c.last_name, c.country, c.postal_code, c.phone, c.email " +
                "FROM customer c";
        List<CustomerShortDto> customers = new ArrayList<>();
        try(Connection conn = DriverManager.getConnection(url);
            PreparedStatement ps = conn.prepareStatement(sql)){
            try (ResultSet rs = ps.executeQuery()){
                while(rs.next()){
                    customers.add(new CustomerShortDto(
                            rs.getInt("customer_id"),
                            rs.getString("first_name"),
                            rs.getString("last_name"),
                            rs.getString("country"),
                            rs.getString("postal_code"),
                            rs.getString("phone"),
                            rs.getString("email")));
                }
            }
        } catch(SQLException e){
            System.out.println(e.getMessage());
        }
        return customers;
    }

    public CustomerShortDto getCustomer(int customerId){
        String sql = "SELECT c.customer_id, c.first_name, c.last_name, c.country, c.postal_code, c.phone, c.email " +
                "FROM customer c " +
                "WHERE c.customer_id = ?";
        CustomerShortDto customer = null;

        try(Connection conn = DriverManager.getConnection(url);
            PreparedStatement ps = conn.prepareStatement(sql)){
            ps.setInt(1, customerId);

            try (ResultSet rs = ps.executeQuery()){
                while(rs.next()){
                    customer = new CustomerShortDto(
                            rs.getInt("customer_id"),
                            rs.getString("first_name"),
                            rs.getString("last_name"),
                            rs.getString("country"),
                            rs.getString("postal_code"),
                            rs.getString("phone"),
                            rs.getString("email"));
                }
            }
        } catch(SQLException e){
            System.out.println(e.getMessage());
        }
        return customer;
    }

    public List<CustomerShortDto> searchCustomers(String searchTerm){
        String sql = "SELECT c.customer_id, c.first_name, c.last_name, c.country, c.postal_code, c.phone, c.email " +
                "FROM customer c " +
                "WHERE c.first_name LIKE ?";
        List<CustomerShortDto> customers = new ArrayList<>();

        try(Connection conn = DriverManager.getConnection(url);
            PreparedStatement ps = conn.prepareStatement(sql)){
            ps.setString(1, "%" + searchTerm + "%");
            try (ResultSet rs = ps.executeQuery()){
                while(rs.next()){
                    customers.add(new CustomerShortDto(
                            rs.getInt("customer_id"),
                            rs.getString("first_name"),
                            rs.getString("last_name"),
                            rs.getString("country"),
                            rs.getString("postal_code"),
                            rs.getString("phone"),
                            rs.getString("email")));
                }
            }
        } catch(SQLException e){
            System.out.println(e.getMessage());
        }
        return customers;
    }

    public List<CustomerShortDto> getRangeOfCustomers(int limit, int offset){
        String sql = "SELECT c.customer_id, c.first_name, c.last_name, c.country, c.postal_code, c.phone, c.email " +
                "FROM customer c " +
                "LIMIT ? OFFSET ?";
        List<CustomerShortDto> customers = new ArrayList<>();

        try(Connection conn = DriverManager.getConnection(url);
            PreparedStatement ps = conn.prepareStatement(sql)){
            ps.setInt(1, limit);
            ps.setInt(2, offset);
            try (ResultSet rs = ps.executeQuery()){
                while(rs.next()){
                    customers.add(new CustomerShortDto(
                            rs.getInt("customer_id"),
                            rs.getString("first_name"),
                            rs.getString("last_name"),
                            rs.getString("country"),
                            rs.getString("postal_code"),
                            rs.getString("phone"),
                            rs.getString("email")));
                }
            }
        } catch(SQLException e){
            System.out.println(e.getMessage());
        }
        return customers;
    }

    public List<CustomerPerCountryDto> getCustomersPerCountry(){
        String sql = "SELECT c.country, COUNT(c.customer_id) as total_customers " +
                "FROM customer c " +
                "GROUP BY c.country " +
                "ORDER BY total_customers DESC";
        List<CustomerPerCountryDto> countries = new ArrayList<>();

        try(Connection conn = DriverManager.getConnection(url);
            PreparedStatement ps = conn.prepareStatement(sql)){
            try (ResultSet rs = ps.executeQuery()){
                while(rs.next()){
                    countries.add(new CustomerPerCountryDto(
                            rs.getString("country"),
                            rs.getInt("total_customers")));
                }
            }
        } catch(SQLException e){
            System.out.println(e.getMessage());
        }
        return countries;

    }

    public List<CustomerSpendingsDto> getCustomerSpendings(){
        String sql = "SELECT c.customer_id, c.first_name, c.last_name, i.total as money_spent " +
                "FROM customer c " +
                "INNER JOIN invoice i ON i.customer_id = c.customer_id " +
                "ORDER BY i.total DESC";
        List<CustomerSpendingsDto> spendings = new ArrayList<>();

        try(Connection conn = DriverManager.getConnection(url);
            PreparedStatement ps = conn.prepareStatement(sql)){
            try (ResultSet rs = ps.executeQuery()){
                while(rs.next()){
                    spendings.add(new CustomerSpendingsDto(
                            rs.getInt("customer_id"),
                            rs.getString("first_name"),
                            rs.getString("last_name"),
                            rs.getDouble("money_spent")));
                }
            }
        } catch(SQLException e){
            System.out.println(e.getMessage());
        }
        return spendings;
    }

    public List<CustomerFavoriteGenreDto> getCustomerFavoriteGenre(int customerId){
        String sql = "SELECT c.customer_id, c.first_name, c.last_name, g.\"name\" genre, COUNT(g.genre_id) genre_count " +
                "FROM customer c " +
                "INNER JOIN invoice inv ON c.customer_id = inv.customer_id " +
                "INNER JOIN invoice_line il ON inv.invoice_id = il.invoice_id " +
                "INNER JOIN track t ON t.track_id = il.track_id " +
                "INNER JOIN genre g ON g.genre_id = t.genre_id " +
                "WHERE c.customer_id = ? " +
                "GROUP BY c.customer_id, c.first_name, c.last_name, g.\"name\", g.genre_id " +
                "ORDER BY genre_count DESC FETCH FIRST 1 ROWS WITH TIES";

        List<CustomerFavoriteGenreDto> favoriteGenre = new ArrayList<>();

        try(Connection conn = DriverManager.getConnection(url);
            PreparedStatement ps = conn.prepareStatement(sql)){
            ps.setInt(1, customerId);
            try (ResultSet rs = ps.executeQuery()){
                while(rs.next()){
                    favoriteGenre.add(new CustomerFavoriteGenreDto(
                            rs.getInt("customer_id"),
                            rs.getString("first_name"),
                            rs.getString("last_name"),
                            rs.getString("genre"),
                            rs.getInt("genre_count")));
                }
            }
        } catch(SQLException e){
            System.out.println(e.getMessage());
        }
        return favoriteGenre;
    }

    public CustomerShortDto createCustomer(CustomerShortDto customer){
        String sql = "INSERT INTO customer(first_name, last_name, country, postal_code, phone, email ) " +
                "VALUES (?, ?, ?, ?, ?, ? ) " +
                "RETURNING (customer_id)";
        int customerId = 0;

        try(Connection conn = DriverManager.getConnection(url);
            PreparedStatement ps = conn.prepareStatement(sql)){
            ps.setString(1, customer.getFirstName());
            ps.setString(2, customer.getLastName());
            ps.setString(3, customer.getCountry());
            ps.setString(4, customer.getPostal_code());
            ps.setString(5, customer.getPhone());
            ps.setString(6, customer.getEmail());

            try(ResultSet rs = ps.executeQuery()){
                rs.next();
                customerId = rs.getInt("customer_id");
            }

        } catch(SQLException e){
            System.out.println(e.getMessage());
        }
        return getCustomer(customerId);
    }

    public CustomerShortDto updateCustomerCountry(CustomerCountryDto updatedCountry, int customerId){
        String sql = "UPDATE customer SET country = ? " +
                "WHERE customer_id = ?";

        try(Connection conn = DriverManager.getConnection(url);
            PreparedStatement ps = conn.prepareStatement(sql)){
            ps.setString(1, updatedCountry.getCountry());
            ps.setInt(2, updatedCountry.getCustomerId());
            ResultSet rs = ps.executeQuery();
        } catch(SQLException e){
            System.out.println(e.getMessage());
        }
        return getCustomer(customerId);
    }

}
