package scania.sebastian.task6_2.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import scania.sebastian.task6_2.dataaccess.ChinookRepository;
import scania.sebastian.task6_2.models.dto.CustomerShortDto;
import scania.sebastian.task6_2.models.dto.CustomerCountryDto;
import scania.sebastian.task6_2.models.dto.CustomerFavoriteGenreDto;
import scania.sebastian.task6_2.models.dto.CustomerSpendingsDto;
import scania.sebastian.task6_2.models.dto.CustomerPerCountryDto;

import java.util.List;

@RestController
@RequestMapping(value="/chinook/customers")
public class ChinookController {
    private ChinookRepository chinookRepository;

    @Autowired
    public ChinookController(ChinookRepository chinookRepository){
        this.chinookRepository = chinookRepository;
    }

    //SQL task 1
    @GetMapping("")
    public List<CustomerShortDto> getAllCustomers() {
        return chinookRepository.getCustomers();
    }

    //SQL task 2
    @GetMapping("/{id}")
    public CustomerShortDto getCustomer(@PathVariable int id) {
        return chinookRepository.getCustomer(id);
    }

    //SQL task 3
    @GetMapping("/search")
    public List<CustomerShortDto> searchCustomers(@RequestParam("query") String terms) {
        return chinookRepository.searchCustomers(terms);
    }

    //SQL task 4
    @GetMapping("/{limit}/{offset}")
    public List<CustomerShortDto> getRangeOfCustomers(@PathVariable int limit, @PathVariable int offset) {
        return chinookRepository.getRangeOfCustomers(limit, offset);
    }

    //SQL task 7
    @GetMapping("/countries")
    public List<CustomerPerCountryDto> getCustomersPerCountry() {
        return chinookRepository.getCustomersPerCountry();
    }

    //SQL task 8
    @GetMapping("/spendings")
    public List<CustomerSpendingsDto> getCustomerSpendings() {
        return chinookRepository.getCustomerSpendings();
    }

    //SQL task 9
    @GetMapping("/{id}/favoriteGenre")
    public List<CustomerFavoriteGenreDto> getCustomerFavoriteGenre(@PathVariable int id) {
        return chinookRepository.getCustomerFavoriteGenre(id);
    }

    //SQL task 5
    @PostMapping("")
    public CustomerShortDto createCustomer(@RequestBody CustomerShortDto customer){
        return chinookRepository.createCustomer(customer);
    }

    //SQL task 6
    @PatchMapping("{id}/country")
    public CustomerShortDto updateCustomerCountry(@RequestBody CustomerCountryDto updatedCountry, @PathVariable int id){
        if(updatedCountry.getCustomerId() != id)
            return null;
        return chinookRepository.updateCustomerCountry(updatedCountry, id);
    }

}
